DROP SCHEMA IF EXISTS MY_DATABASE CASCADE;
CREATE SCHEMA MY_DATABASE;

DROP TABLE IF EXISTS MY_DATABASE.SUPPLIER;
CREATE TABLE MY_DATABASE.SUPPLIER
(
    publisher_nm VARCHAR(200) PRIMARY KEY,
    company_nm   VARCHAR(200) NOT NULL
);

DROP TABLE IF EXISTS MY_DATABASE.BOOK;
CREATE TABLE MY_DATABASE.BOOK
(
    book_id      SERIAL PRIMARY KEY,
    author_nm    VARCHAR(200)                                                NOT NULL,
    book_nm      VARCHAR(200)                                                NOT NULL,
    publisher_nm VARCHAR(200) REFERENCES MY_DATABASE.SUPPLIER (publisher_nm) NOT NULL,
    cost_amt     INT CHECK ( cost_amt > 0 )
);

DROP TABLE IF EXISTS MY_DATABASE.CONSUMER CASCADE;
CREATE TABLE MY_DATABASE.CONSUMER
(
    consumer_id SERIAL PRIMARY KEY,
    consumer_nm VARCHAR(200) NOT NULL,
    town_nm     VARCHAR(200),
    phone_nm    VARCHAR(12) DEFAULT NULL
);

DROP TABLE IF EXISTS MY_DATABASE.ORDER CASCADE ;
CREATE TABLE MY_DATABASE.ORDER
(
    order_id      SERIAL PRIMARY KEY,
    consumer_id   INT REFERENCES MY_DATABASE.CONSUMER (consumer_id) NOT NULL,
    order_dt      DATE    DEFAULT CURRENT_DATE,
    delivered_flg BOOLEAN DEFAULT FALSE
);

DROP TABLE IF EXISTS MY_DATABASE.STORAGE;
CREATE TABLE MY_DATABASE.STORAGE
(
    place_id       SERIAL PRIMARY KEY,
    row_no         INT CHECK ( row_no > 0 AND row_no <= 100 ),
    shelf_no       INT CHECK ( shelf_no > 0 AND shelf_no <= 6 ),
    shelf_space_no INT CHECK ( shelf_space_no > 0 AND shelf_space_no <= 20 )
);

DROP TABLE IF EXISTS MY_DATABASE.ORDER_X_BOOK CASCADE ;
CREATE TABLE MY_DATABASE.ORDER_X_BOOK
(
    order_id INT REFERENCES MY_DATABASE.ORDER (order_id) NOT NULL,
    book_id  INT REFERENCES MY_DATABASE.BOOK (book_id)   NOT NULL,
    book_cnt INT DEFAULT 1 CHECK ( book_cnt > 0 ),
    CONSTRAINT PK_ORDER_X_BOOK PRIMARY KEY (order_id, book_id)
);

DROP TABLE IF EXISTS MY_DATABASE.REVIEW CASCADE ;
CREATE TABLE MY_DATABASE.REVIEW
(
    book_id     INT REFERENCES MY_DATABASE.BOOK (book_id)         NOT NULL,
    consumer_id INT REFERENCES MY_DATABASE.CONSUMER (consumer_id) NOT NULL,
    review_txt  VARCHAR(10),
    CONSTRAINT PK_REVIEW PRIMARY KEY (book_id, consumer_id)
);

DROP TABLE IF EXISTS MY_DATABASE.BOOK_X_STORAGE;
CREATE TABLE MY_DATABASE.BOOK_X_STORAGE
(
    book_id  INT REFERENCES MY_DATABASE.BOOK (book_id)     NOT NULL,
    place_id INT REFERENCES MY_DATABASE.STORAGE (place_id) NOT NULL,
    PRIMARY KEY (place_id)
);


UPDATE MY_DATABASE.CONSUMER
    SET phone_nm = NULL
WHERE phone_nm = '';


INSERT INTO MY_DATABASE.SUPPLIER
VALUES ('АСТ', 'ООО "Довезём!"'),
       ('Эксмо', 'ООО "Довезём!"'),
       ('Феникс', 'ООО "Довезём!"'),
       ('Азбука', 'ИП Сабиров А.А.'),
       ('Стрекоза', 'ИП Сабиров А.А.'),
       ('Время', 'Издательство "Время"'),
       ('Речь', 'ООО "Premium"'),
       ('Альфа-книга', 'ООО "Premium"');

INSERT INTO MY_DATABASE.BOOK (author_nm, book_nm, publisher_nm, cost_amt)
VALUES ('Толстой Л.Н.', 'Война и мир', 'Речь', 6968),
       ('Толстой Л.Н.', 'Война и мир', 'Время', 1488),
       ('Толстой Л.Н.', 'Война и мир', 'Эксмо', 360),
       ('Толстой Л.Н.', 'Война и мир', 'АСТ', 474),
       ('Толстой Л.Н.', 'Севастопольские рассказы', 'Время', 272),
       ('Толстой Л.Н.', 'Севастопольские рассказы', 'Речь', 799),
       ('Толстой Л.Н.', 'Севастопольские рассказы', 'Стрекоза', 233),
       ('Толстой Л.Н.', 'Смерть Ивана Ильича', 'Эксмо', 149),
       ('Толстой Л.Н.', 'Смерть Ивана Ильича', 'Время', 256),
       ('Толстой Л.Н.', 'Смерть Ивана Ильича', 'Азбука', 120),
       ('Толстой Л.Н.', 'Кавказский пленник', 'АСТ', 247),
       ('Толстой Л.Н.', 'Кавказский пленник', 'Речь', 409),
       ('Толстой Л.Н.', 'Кавказский пленник', 'Стрекоза', 91),
       ('Лермонтов М.Ю.', 'Кавказский пленник', 'Эксмо', 540),
       ('Пушкин А.С', 'Кавказский пленник', 'АСТ', 46),
       ('Пушкин А.С', 'Пиковая дама', 'Эксмо', 169),
       ('Пушкин А.С', 'Пиковая дама', 'АСТ', 694),
       ('Пушкин А.С', 'Пиковая дама', 'Речь', 799),
       ('Пушкин А.С', 'Евгений Онегин', 'Стрекоза', 140),
       ('Пушкин А.С', 'Евгений Онегин', 'Время', 288),
       ('Пушкин А.С', 'Евгений Онегин', 'АСТ', 1487),
       ('Пушкин А.С', 'Евгений Онегин', 'Речь', 1601),
       ('Пушкин А.С', 'Евгений Онегин', 'Азбука', 239),
       ('Пушкин А.С', 'Сказки', 'Стрекоза', 307),
       ('Пушкин А.С', 'Сказки', 'Речь', 184),
       ('Пушкин А.С', 'Стихи', 'Речь', 558),
       ('Пушкин А.С', 'Стихи', 'Эксмо', 410),
       ('Лермонтов М.Ю.', 'Герой нашего времени', 'АСТ', 160),
       ('Лермонтов М.Ю.', 'Герой нашего времени', 'Речь', 790),
       ('Лермонтов М.Ю.', 'Герой нашего времени', 'Время', 208),
       ('Лермонтов М.Ю.', 'Герой нашего времени', 'Стрекоза', 229),
       ('Лермонтов М.Ю.', 'Герой нашего времени', 'Эксмо', 275),
       ('Лермонтов М.Ю.', 'Стихи', 'Речь', 648),
       ('Лермонтов М.Ю.', 'Стихи', 'АСТ', 895),
       ('Лермонтов М.Ю.', 'Стихи', 'Азбука', 86);

INSERT INTO MY_DATABASE.ORDER (consumer_id, order_dt, delivered_flg)
VALUES (1, '2019-04-27', TRUE),
       (1, '2019-04-30', TRUE),
       (1, '2019-05-03', FALSE),
       (2, '2019-04-25', TRUE),
       (3, '2019-04-29', FALSE),
       (4, '2019-04-25', TRUE),
       (4, '2019-04-30', TRUE),
       (5, '2019-04-29', FALSE),
       (5, '2019-05-03', FALSE),
       (6, '2019-04-21', TRUE),
       (6, '2019-04-27', FALSE),
       (6, '2019-05-03', FALSE);


-- вывести все доставленные книги, а также их покупателей
SELECT consumer_nm, "order".order_id, book_nm, author_nm, book_cnt
FROM MY_DATABASE.CONSUMER
         JOIN MY_DATABASE.ORDER
              ON MY_DATABASE.CONSUMER.consumer_id = MY_DATABASE.ORDER.consumer_id
         JOIN MY_DATABASE.ORDER_X_BOOK
              ON MY_DATABASE.ORDER.order_id = MY_DATABASE.ORDER_X_BOOK.order_id
         JOIN MY_DATABASE.BOOK
              ON order_x_book.book_id = book.book_id
WHERE delivered_flg = TRUE;


-- найти людей, которые уже получили книги, но не оставили ни одного отзыва
SELECT DISTINCT consumer.consumer_id, consumer_nm
FROM MY_DATABASE.CONSUMER
         JOIN MY_DATABASE.ORDER
              ON consumer.consumer_id = "order".consumer_id
WHERE delivered_flg = TRUE
    EXCEPT
SELECT DISTINCT consumer.consumer_id, consumer_nm
FROM MY_DATABASE.CONSUMER
         JOIN MY_DATABASE.REVIEW
              ON consumer.consumer_id = review.consumer_id
ORDER BY consumer_id;


-- найти книги (авторов), о которых не оставили ни одного отзыва
SELECT DISTINCT author_nm
              , book_nm
FROM MY_DATABASE.BOOK
    EXCEPT
SELECT DISTINCT author_nm
              , book_nm
FROM MY_DATABASE.BOOK
         JOIN MY_DATABASE.REVIEW
              ON book.book_id = review.book_id
ORDER BY author_nm;

-- найти издательства, у которых не заказали ни одной книги
SELECT DISTINCT publisher_nm
FROM MY_DATABASE.BOOK
    EXCEPT
SELECT DISTINCT publisher_nm
FROM MY_DATABASE.BOOK
         JOIN MY_DATABASE.ORDER_X_BOOK
              ON book.book_id = order_x_book.book_id;


-- вывести книги, которых нет на складе
WITH MISSING_BOOKS AS (
    SELECT book_id, author_nm, book_nm, publisher_nm
    FROM MY_DATABASE.BOOK
        EXCEPT
    SELECT DISTINCT book.book_id, author_nm, book_nm, publisher_nm
    FROM MY_DATABASE.BOOK
             JOIN MY_DATABASE.BOOK_X_STORAGE
                  ON book.book_id = book_x_storage.book_id
    ORDER BY book_id
)
-- SELECT count(book_id) AS cnt FROM MISSING_BOOKS;
SELECT *
FROM MISSING_BOOKS;

-- количество свободных мест на складе
WITH EMPTY_SPACE AS (
    SELECT place_id, row_no, shelf_no, shelf_space_no
    FROM MY_DATABASE.STORAGE
        EXCEPT
    SELECT storage.place_id, row_no, shelf_no, shelf_space_no
    FROM MY_DATABASE.BOOK_X_STORAGE
             JOIN MY_DATABASE.STORAGE
                  ON book_x_storage.place_id = storage.place_id
    ORDER BY place_id
)
-- SELECT * FROM EMPTY_SPACE;
SELECT count(place_id)
FROM EMPTY_SPACE;


-- найти заказы, в которых все книги одного автора
WITH ORDERS AS (
    SELECT DISTINCT order_id, author_nm
    FROM MY_DATABASE.ORDER_X_BOOK
             JOIN MY_DATABASE.BOOK
                  ON order_x_book.book_id = book.book_id
    ORDER BY order_id
)
SELECT new_order_id, author_nm
FROM (
         SELECT order_id AS new_order_id, count(author_nm) AS cnt
         FROM ORDERS
         GROUP BY new_order_id
     ) AS my_TABLE
         JOIN ORDERS
              ON new_order_id = order_id
WHERE cnt = 1
ORDER BY order_id;

-- найти человека, который оставил больше всего отзывов
WITH STATISTIC AS (
    SELECT consumer_id AS new_consumer_id, count(book_id) AS cnt
    FROM MY_DATABASE.REVIEW
    GROUP BY new_consumer_id
)
SELECT consumer_nm, cnt
FROM STATISTIC
         JOIN MY_DATABASE.CONSUMER
              ON new_consumer_id = consumer_id
WHERE cnt = (SELECT max(cnt) FROM STATISTIC);

-- найти книгу, о которой оставлено больше всего отзывов
WITH STATISTIC AS (
    SELECT book_id AS new_book_id, count(book_id) AS cnt
    FROM MY_DATABASE.REVIEW
    GROUP BY new_book_id
)
SELECT author_nm, book_nm, publisher_nm, cnt AS review_cnt
FROM STATISTIC
         JOIN MY_DATABASE.BOOK
              ON new_book_id = book_id
WHERE cnt = (SELECT max(cnt) FROM STATISTIC);

-- найти книгу, о которой оставлено больше всего отзывов (разные издательства не имеют значения)
WITH STATISTIC AS (
    SELECT author_nm, book_nm, count(book_nm) AS cnt
    FROM MY_DATABASE.REVIEW
             JOIN MY_DATABASE.BOOK
                  ON review.book_id = book.book_id
    GROUP BY (author_nm, book_nm)
)
SELECT *
FROM STATISTIC
WHERE cnt = (SELECT max(cnt) FROM STATISTIC);


-- CRUD-запрос к таблице ORDER
INSERT INTO MY_DATABASE.ORDER (consumer_id) VALUES (2)
RETURNING order_id;

SELECT * FROM MY_DATABASE.ORDER;

UPDATE MY_DATABASE.ORDER
SET delivered_flg = TRUE
WHERE order_id >= ALL (SELECT order_id FROM MY_DATABASE.ORDER);

DELETE FROM MY_DATABASE.ORDER
WHERE order_dt = CURRENT_DATE;


-- CRUD-запрос к таблице ORDER_X_BOOK
INSERT INTO MY_DATABASE.ORDER_X_BOOK (order_id, book_id) VALUES (13, 15);

SELECT * FROM MY_DATABASE.ORDER_X_BOOK;

UPDATE MY_DATABASE.ORDER_X_BOOK
SET book_cnt = 2
WHERE order_id = 13;

DELETE FROM MY_DATABASE.ORDER_X_BOOK
WHERE order_id = 13;


DROP VIEW IF EXISTS MY_DATABASE.BOOKS_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.BOOKS_VIEW AS
SELECT row_number() over (PARTITION BY publisher_nm ORDER BY author_nm, book_nm), author_nm, book_nm, publisher_nm
FROM MY_DATABASE.BOOK;

DROP VIEW IF EXISTS MY_DATABASE.BOOK_X_STORAGE_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.BOOK_X_STORAGE_VIEW AS
SELECT author_nm, book_nm, publisher_nm, row_no, shelf_no, shelf_space_no
FROM MY_DATABASE.STORAGE
         JOIN MY_DATABASE.BOOK_X_STORAGE
              ON storage.place_id = book_x_storage.place_id
         JOIN MY_DATABASE.BOOK
              ON book_x_storage.book_id = book.book_id;

DROP VIEW IF EXISTS MY_DATABASE.ORDER_X_BOOK_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.ORDER_X_BOOK_VIEW AS
SELECT order_dt, author_nm, book_nm, sum(book_cnt)
FROM MY_DATABASE.ORDER
         JOIN MY_DATABASE.ORDER_X_BOOK
              ON "order".order_id = order_x_book.order_id
         JOIN MY_DATABASE.BOOK
              ON order_x_book.book_id = book.book_id
GROUP BY order_dt, author_nm, book_nm
ORDER BY order_dt, author_nm;

DROP VIEW IF EXISTS MY_DATABASE.ORDER_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.ORDER_VIEW AS
SELECT order_id, consumer_id
FROM MY_DATABASE.ORDER
WHERE delivered_flg = FALSE
ORDER BY order_dt
    WITH LOCAL CHECK OPTION;

DROP VIEW IF EXISTS MY_DATABASE.REVIEW_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.REVIEW_VIEW AS
SELECT book_id, review_txt
FROM MY_DATABASE.REVIEW
ORDER BY book_id;

DROP VIEW IF EXISTS MY_DATABASE.CONSUMER_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.CONSUMER_VIEW AS
SELECT consumer_id,
       consumer_nm,
       town_nm,
       CASE
           WHEN phone_nm is NULL
               THEN NULL
           ELSE CONCAT('8******', SUBSTR(phone_nm, 8, 4))
           END pnone_no
FROM MY_DATABASE.CONSUMER;

DROP VIEW IF EXISTS MY_DATABASE.SUPPLIER_VIEW;
CREATE OR REPLACE VIEW MY_DATABASE.SUPPLIER_VIEW AS
SELECT company_nm, publisher_nm
FROM MY_DATABASE.SUPPLIER
WHERE company_nm LIKE 'ООО %'
WITH LOCAL CHECK OPTION;


CREATE FUNCTION MY_DATABASE.order_cost(orderID INT)
RETURNS BIGINT
AS $$
    SELECT SUM(book_cnt * cost_amt)
    FROM ORDER_X_BOOK JOIN BOOK
    ON order_x_book.book_id = book.book_id
    WHERE order_id = orderID;
$$ LANGUAGE SQL;

SELECT MY_DATABASE.order_cost(3);

CREATE FUNCTION MY_DATABASE.delivery(town VARCHAR(200))
RETURNS TABLE(bookID INT, bookCNT BIGINT)
AS $$
    SELECT book_id, SUM(book_cnt)
    FROM MY_DATABASE.CONSUMER JOIN MY_DATABASE.ORDER
    ON consumer.consumer_id = "order".consumer_id
    JOIN MY_DATABASE.ORDER_X_BOOK OXB
    ON "order".order_id = OXB.order_id
    WHERE town_nm = town AND delivered_flg = FALSE
    GROUP BY book_id
$$ LANGUAGE SQL;

SELECT MY_DATABASE.delivery('Москва');